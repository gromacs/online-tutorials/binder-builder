FROM docker:20

# The base image is based on alpine, so use its apk package manager
RUN apk add python3 py3-pip

# Install the common requirements for building binder images
RUN pip3 install jupyter-repo2docker
