# binder-builder

Contains a `Dockerfile` for images that are capable of building Docker images on GitLab Runner CI.
Such binder-builder images are built by GitLab Runner CI in `gitlab-ci.yml` and stored on GitLab's container registry.
A container made from a stored image is used runs the GitLab Runner CI for the tutorial repositories.
There, they use the repo2docker package added by this `Dockerfile` to build further images that contain a binder of their notebook.
Those images will also be stored on GitLab's container registry, so that later mybinder will launch binders quickly.

Building Docker containers within Docker containers requires particular configuration.
Fortunately, the shared runners on GitLab permit this by default.
The GROMACS runners do not have this configured.
Thus, this requires that Settings > CI/CD > Runners has "shared runners" enabled and "group runners" disabled.
